var indexSectionsWithContent =
{
  0: "3abcdefhilmoprstuvw",
  1: "3boprt",
  2: "acdhilmrsuw",
  3: "abcdefhimprstv",
  4: "bcmoprs",
  5: "bc",
  6: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines",
  5: "groups",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Macros",
  5: "Modules",
  6: "Pages"
};

