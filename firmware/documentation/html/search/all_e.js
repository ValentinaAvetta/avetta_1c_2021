var searchData=
[
  ['salida_5ffiltrada_88',['salida_filtrada',['../_osciloscope___proyecto4___e_c_g_8c.html#abcf8839f80e5c53fc91751fa3f941e89',1,'Osciloscope_Proyecto4_ECG.c']]],
  ['salida_5ffiltrada_5fanterior_89',['salida_filtrada_anterior',['../_osciloscope___proyecto4___e_c_g_8c.html#af57ee2e6d965cf28aaeadbd1efaf40e0',1,'Osciloscope_Proyecto4_ECG.c']]],
  ['sample_90',['sample',['../_recuperatorio_avetta1_8c.html#a557cbeabffc4ae613e1db945c13d140b',1,'sample():&#160;RecuperatorioAvetta1.c'],['../_recuperatorio_avetta2_8c.html#a557cbeabffc4ae613e1db945c13d140b',1,'sample():&#160;RecuperatorioAvetta2.c'],['../_temperature_sensor_8c.html#a557cbeabffc4ae613e1db945c13d140b',1,'sample():&#160;TemperatureSensor.c']]],
  ['sampling_91',['Sampling',['../_recuperatorio_avetta1_8c.html#a8d5baa0271a9ff104b1cd117510dd4c9',1,'Sampling(void):&#160;RecuperatorioAvetta1.c'],['../_recuperatorio_avetta2_8c.html#a8d5baa0271a9ff104b1cd117510dd4c9',1,'Sampling(void):&#160;RecuperatorioAvetta2.c'],['../_temperature_sensor_8h.html#a8d5baa0271a9ff104b1cd117510dd4c9',1,'Sampling(void):&#160;RecuperatorioAvetta1.c'],['../_temperature_sensor_8c.html#a8d5baa0271a9ff104b1cd117510dd4c9',1,'Sampling(void):&#160;TemperatureSensor.c']]],
  ['showpromtemp_92',['ShowPromTemp',['../_temperature_sensor_8h.html#a435e16e0365ad7306ea8942a821022a2',1,'ShowPromTemp(void):&#160;TemperatureSensor.c'],['../_temperature_sensor_8c.html#a435e16e0365ad7306ea8942a821022a2',1,'ShowPromTemp(void):&#160;TemperatureSensor.c']]],
  ['sisinit_93',['SisInit',['../blinking__switch__timer_8c.html#a46bf1fb2cbe45b28feb171dfd65028c4',1,'blinking_switch_timer.c']]],
  ['sound_94',['Sound',['../_temperature_sensor_8h.html#a9bf43a6adfdd3959cea6e49987a30a8b',1,'Sound(void):&#160;TemperatureSensor.c'],['../_temperature_sensor_8c.html#a9bf43a6adfdd3959cea6e49987a30a8b',1,'Sound(void):&#160;TemperatureSensor.c']]],
  ['state_95',['state',['../_parcial_avetta2021_8c.html#a0b57aa10271a66f3dc936bba1d2f3830',1,'state():&#160;ParcialAvetta2021.c'],['../_tcrt5000___proyecto3_8c.html#ab30ba07e2a0bd07a15e45a92c32db9c5',1,'state():&#160;Tcrt5000_Proyecto3.c']]],
  ['stateonoff_96',['stateOnOff',['../_parcial_avetta2021_8c.html#a439db20cbda307c67df0fda9dee3971c',1,'ParcialAvetta2021.c']]],
  ['sub_5fmaxtemp_97',['SUB_MAXTEMP',['../_temperature_sensor_8c.html#af925d26b20e0267920ed83710c558432',1,'TemperatureSensor.c']]],
  ['sub_5fmintemp_98',['SUB_MINTEMP',['../_temperature_sensor_8c.html#a311e531be23fe9a2aabc72aa5ed0ff87',1,'TemperatureSensor.c']]],
  ['sysinit_99',['SysInit',['../3___uart_r_s232a_u_s_b_8c.html#ad043a980b1a4d8b680ddaf8cf137d619',1,'3_UartRS232aUSB.c']]]
];
