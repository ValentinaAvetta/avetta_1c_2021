var searchData=
[
  ['calculatealfa_17',['CalculateAlfa',['../_osciloscope___proyecto4___e_c_g_8h.html#ae47ed1b794c9f0a5082c2645f8682095',1,'CalculateAlfa(void):&#160;Osciloscope_Proyecto4_ECG.c'],['../_osciloscope___proyecto4___e_c_g_8c.html#ae47ed1b794c9f0a5082c2645f8682095',1,'CalculateAlfa(void):&#160;Osciloscope_Proyecto4_ECG.c']]],
  ['calculavelocidad_18',['CalculaVelocidad',['../_parcial_avetta2021_8h.html#acccb7250e099cf749f755243735ddeb1',1,'CalculaVelocidad(void):&#160;ParcialAvetta2021.c'],['../_parcial_avetta2021_8c.html#acccb7250e099cf749f755243735ddeb1',1,'CalculaVelocidad(void):&#160;ParcialAvetta2021.c']]],
  ['cero_19',['CERO',['../_parcial_avetta2021__2_8c.html#a7558fe3a1073e3ed554281ff249a4987',1,'ParcialAvetta2021_2.c']]],
  ['ciaa_20firmware_20',['CIAA Firmware',['../group___c_i_a_a___firmware.html',1,'']]],
  ['ciaa_20firmware_20examples_21',['CIAA Firmware Examples',['../group___examples.html',1,'']]],
  ['configrealtime_22',['ConfigRealTime',['../_temperature_sensor_8c.html#a9a4282fd4fc45e61646e7ec29d90e4f7',1,'TemperatureSensor.c']]],
  ['conttobinary_23',['contToBinary',['../_tcrt5000___proyecto_8h.html#aa8f47b9d70ab4188a667cfe5e710511e',1,'contToBinary(uint8_t contador):&#160;Tcrt5000_Proyecto.c'],['../_tcrt5000___proyecto_8c.html#aa8f47b9d70ab4188a667cfe5e710511e',1,'contToBinary(uint8_t contador):&#160;Tcrt5000_Proyecto.c']]],
  ['count_24',['count',['../_parcial_avetta2021__2_8c.html#a20302e2c99a60d3f612dba57e3f6333b',1,'count():&#160;ParcialAvetta2021_2.c'],['../_recuperatorio_avetta1_8c.html#a20302e2c99a60d3f612dba57e3f6333b',1,'count():&#160;RecuperatorioAvetta1.c'],['../_recuperatorio_avetta2_8c.html#a20302e2c99a60d3f612dba57e3f6333b',1,'count():&#160;RecuperatorioAvetta2.c'],['../_tcrt5000___proyecto3_8c.html#a20302e2c99a60d3f612dba57e3f6333b',1,'count():&#160;Tcrt5000_Proyecto3.c'],['../_temperature_sensor_8c.html#a20302e2c99a60d3f612dba57e3f6333b',1,'count():&#160;TemperatureSensor.c']]],
  ['count_5fdelay_25',['COUNT_DELAY',['../1__blinking__switch_2src_2blinking__switch_8c.html#aa655f66de5a285ff5fc8fc2710758f63',1,'COUNT_DELAY():&#160;blinking_switch.c'],['../2__blinking__switch_2src_2blinking__switch_8c.html#aa655f66de5a285ff5fc8fc2710758f63',1,'COUNT_DELAY():&#160;blinking_switch.c']]],
  ['countbin_26',['countBin',['../_tcrt5000___proyecto3_8c.html#ad6b97b4042f64718ab2ed558ec51d1c0',1,'Tcrt5000_Proyecto3.c']]],
  ['cruceporcero_27',['CrucePorCero',['../_parcial_avetta2021__2_8h.html#a0c33142f8dd262352ea8e8a360926750',1,'CrucePorCero(void):&#160;ParcialAvetta2021_2.c'],['../_parcial_avetta2021__2_8c.html#a0c33142f8dd262352ea8e8a360926750',1,'CrucePorCero(void):&#160;ParcialAvetta2021_2.c']]]
];
