var searchData=
[
  ['r0_78',['r0',['../_parcial_avetta2021__2_8c.html#a44084b227ebb9ecd2736e5c21bbaa4b7',1,'ParcialAvetta2021_2.c']]],
  ['rc_79',['RC',['../_osciloscope___proyecto4___e_c_g_8c.html#a36fcf606cfe3585189a19e9aad991969',1,'Osciloscope_Proyecto4_ECG.c']]],
  ['readandfilterecg_80',['ReadAndFilterECG',['../_osciloscope___proyecto4___e_c_g_8h.html#a5566636beef75e1b6d67d793dc59d660',1,'ReadAndFilterECG(void):&#160;Osciloscope_Proyecto4_ECG.c'],['../_osciloscope___proyecto4___e_c_g_8c.html#a5566636beef75e1b6d67d793dc59d660',1,'ReadAndFilterECG(void):&#160;Osciloscope_Proyecto4_ECG.c']]],
  ['readtemperature_81',['ReadTemperature',['../_temperature_sensor_8h.html#a5055661bf32b223db83c9c97f51245b6',1,'ReadTemperature(void):&#160;TemperatureSensor.c'],['../_temperature_sensor_8c.html#a5055661bf32b223db83c9c97f51245b6',1,'ReadTemperature(void):&#160;TemperatureSensor.c']]],
  ['recuperatorioavetta1_2ec_82',['RecuperatorioAvetta1.c',['../_recuperatorio_avetta1_8c.html',1,'']]],
  ['recuperatorioavetta1_2eh_83',['RecuperatorioAvetta1.h',['../_recuperatorio_avetta1_8h.html',1,'']]],
  ['recuperatorioavetta2_2ec_84',['RecuperatorioAvetta2.c',['../_recuperatorio_avetta2_8c.html',1,'']]],
  ['recuperatorioavetta2_2eh_85',['RecuperatorioAvetta2.h',['../_recuperatorio_avetta2_8h.html',1,'']]],
  ['rgb_5fperiod_86',['RGB_PERIOD',['../blinking__switch__timer_8c.html#aaae3dcc2a90bc6e35b3303bd050af326',1,'blinking_switch_timer.c']]],
  ['rs232send_87',['RS232Send',['../3___uart_r_s232a_u_s_b_8c.html#a6e92a14c8d3d57a8dfeebc591361a310',1,'3_UartRS232aUSB.c']]]
];
