var searchData=
[
  ['bandera_6',['bandera',['../_parcial_avetta2021__2_8c.html#ae27e263192f0a38422bd140b1d31f447',1,'ParcialAvetta2021_2.c']]],
  ['bare_20metal_20example_20header_20file_7',['Bare Metal example header file',['../group___baremetal.html',1,'']]],
  ['binarycount_8',['binaryCount',['../_tcrt5000___proyecto_8c.html#a32d984115c942100e82ac72e1a40f7b2',1,'binaryCount():&#160;Tcrt5000_Proyecto.c'],['../_tcrt5000___proyecto3_8c.html#a32d984115c942100e82ac72e1a40f7b2',1,'binaryCount():&#160;Tcrt5000_Proyecto3.c']]],
  ['blinking_9',['blinking',['../blinking__switch__timer_8c.html#af84b8dfa910f3b2b225db4eba400b411',1,'blinking_switch_timer.c']]],
  ['blinking_2ec_10',['blinking.c',['../blinking_8c.html',1,'']]],
  ['blinking_2eh_11',['blinking.h',['../blinking_8h.html',1,'']]],
  ['blinking_5fswitch_2ec_12',['blinking_switch.c',['../1__blinking__switch_2src_2blinking__switch_8c.html',1,'(Global Namespace)'],['../2__blinking__switch_2src_2blinking__switch_8c.html',1,'(Global Namespace)'],['../2__test_r_t_c_2src_2blinking__switch_8c.html',1,'(Global Namespace)']]],
  ['blinking_5fswitch_2eh_13',['blinking_switch.h',['../1__blinking__switch_2inc_2blinking__switch_8h.html',1,'(Global Namespace)'],['../2__blinking__switch_2inc_2blinking__switch_8h.html',1,'(Global Namespace)'],['../2__test_r_t_c_2inc_2blinking__switch_8h.html',1,'(Global Namespace)']]],
  ['blinking_5fswitch_5ftimer_2ec_14',['blinking_switch_timer.c',['../blinking__switch__timer_8c.html',1,'']]],
  ['blinking_5fswitch_5ftimer_2eh_15',['blinking_switch_timer.h',['../blinking__switch__timer_8h.html',1,'']]],
  ['buffer_5fsize_16',['BUFFER_SIZE',['../_osciloscope___proyecto4_8c.html#a6b20d41d6252e9871430c242cb1a56e7',1,'BUFFER_SIZE():&#160;Osciloscope_Proyecto4.c'],['../_osciloscope___proyecto4___e_c_g_8c.html#a6b20d41d6252e9871430c242cb1a56e7',1,'BUFFER_SIZE():&#160;Osciloscope_Proyecto4_ECG.c']]]
];
