var _osciloscope___proyecto4___e_c_g_8c =
[
    [ "BUFFER_SIZE", "_osciloscope___proyecto4___e_c_g_8c.html#a6b20d41d6252e9871430c242cb1a56e7", null ],
    [ "PI", "_osciloscope___proyecto4___e_c_g_8c.html#a598a3330b3c21701223ee0ca14316eca", null ],
    [ "CalculateAlfa", "_osciloscope___proyecto4___e_c_g_8c.html#ae47ed1b794c9f0a5082c2645f8682095", null ],
    [ "InterrupcionTimer", "_osciloscope___proyecto4___e_c_g_8c.html#a1aa17c9a3392ec510a30776e0084c6ca", null ],
    [ "main", "_osciloscope___proyecto4___e_c_g_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "ReadAndFilterECG", "_osciloscope___proyecto4___e_c_g_8c.html#a5566636beef75e1b6d67d793dc59d660", null ],
    [ "WriteECG", "_osciloscope___proyecto4___e_c_g_8c.html#a24bd0fcea5731947223464b1bd77e15e", null ],
    [ "alfa", "_osciloscope___proyecto4___e_c_g_8c.html#a5bf563dfc55db9dac98ac73ed7a8df92", null ],
    [ "ecg", "_osciloscope___proyecto4___e_c_g_8c.html#a2dafb8b8179af8e9f4cc4df3f588e42e", null ],
    [ "entrada", "_osciloscope___proyecto4___e_c_g_8c.html#a55924ae61b9643fb67fa62ae122ba418", null ],
    [ "fc", "_osciloscope___proyecto4___e_c_g_8c.html#a65961d1520c936c93ef3da693d2bcd3b", null ],
    [ "filter", "_osciloscope___proyecto4___e_c_g_8c.html#a5b85fc472953eac39c2629b1f5d68617", null ],
    [ "i", "_osciloscope___proyecto4___e_c_g_8c.html#af27e3188294c2df66d975b74a09c001d", null ],
    [ "mySerial", "_osciloscope___proyecto4___e_c_g_8c.html#a058c0b23c61b13989adca9a867e1f5a8", null ],
    [ "RC", "_osciloscope___proyecto4___e_c_g_8c.html#a36fcf606cfe3585189a19e9aad991969", null ],
    [ "salida_filtrada", "_osciloscope___proyecto4___e_c_g_8c.html#abcf8839f80e5c53fc91751fa3f941e89", null ],
    [ "salida_filtrada_anterior", "_osciloscope___proyecto4___e_c_g_8c.html#af57ee2e6d965cf28aaeadbd1efaf40e0", null ]
];