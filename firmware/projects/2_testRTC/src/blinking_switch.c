/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../2_testRTC/inc/blinking_switch.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "delay.h"
#include "uart.h"
#include "sapi_rtc.h"



/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	LedsInit(); /*Inicializo LEDs con el driver de LEDS */
	SystemClockInit();
	SwitchesInit(); /*Inicializo los switches*/


	uint8_t teclas;
	rtc_t FechaYHora;
	FechaYHora.year = 2021;
	FechaYHora.month = 6;
	FechaYHora.mday = 12;
	FechaYHora.wday = 6;
	FechaYHora.hour=8;
	FechaYHora.min=15;
	FechaYHora.sec=0;  //Configuro la hora

	rtcInit();
	rtcWrite(&FechaYHora);

	serial_config SerieUSB = {SERIAL_PORT_PC,115200,NULL};
	UartInit(&SerieUSB);

	while (1) {

		teclas = SwitchesRead();

		switch (teclas) {

		case SWITCH_1:
			rtcRead(&FechaYHora); //Leo la fecha y hora
			UartSendString(SERIAL_PORT_PC,"Hora: ");
			UartSendString(SERIAL_PORT_PC,UartItoa(FechaYHora.hour,10));
			UartSendString(SERIAL_PORT_PC," : ");
			UartSendString(SERIAL_PORT_PC,UartItoa(FechaYHora.min,10));
			UartSendString(SERIAL_PORT_PC," : ");
			UartSendString(SERIAL_PORT_PC,UartItoa(FechaYHora.sec,10));
			UartSendString(SERIAL_PORT_PC,"\r\n");


			DelayMs(200);
			break;


		}



	}

	return 0;
}




/*==================[end of file]============================================*/

