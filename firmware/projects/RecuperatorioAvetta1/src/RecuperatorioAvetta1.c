/*! @mainpage Recuperatorio Ejercicio 1 -Valentina Avetta
 *
 * \section genDesc General Description
 *
 * Implementacion de firmware de un sistema de medición de volumen, a partir de un sensor de distancia.
 * Se envia el dato 6 veces por segundo por la UART a la PC en el siguiente formato: xx + “ cm3” (uno por renglón).
 * Se utiliza interrupciones de timer. (Para el cálculo de volumen aproxime pi a 3,14).
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	HCSR04		|	T_COL0		|
 * |  				|				|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Document creation		                         |
 * | 18/06/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/RecuperatorioAvetta1.h"       /* <= own header */
#include "gpio.h"
#include "delay.h"
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "HCSR04.h" /*Driver de medidor de distancia*/

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void CalulaVolumen(void);
void DisplayPC(void);
void HardConfig(void);

/*==================[external data definition]===============================*/

#define PI 3.14 /*Defino el valor de PI*/
#define DIAMETRO 6 /*Diamtro del recipeinte en cm*/
#define ALTO 10  /*Alto del recipiente en cm*/

timer_config myTimerA = {TIMER_A, 167, &CalulaVolumen};        /*Periodo de 167 ms, porque fm= 6 --> 1000/fm= 166.66, redondeo a 167 ms*/
serial_config mySerial= {SERIAL_PORT_PC, 115200, NULL};

float volumen;
uint8_t radioAlCuadrado;

/*==================[external functions definition]==========================*/

void CalulaVolumen(void){

	radioAlCuadrado= (DIAMETRO/2)*(DIAMETRO/2);
	volumen= radioAlCuadrado*PI*(ALTO-HcSr04ReadDistanceCentimeters());  /*Formula de volumen de un cilindro --> V=h*pi*r^2 */

	DisplayPC(); /*muetro resultado por pantalla de PC*/
}

void DisplayPC(void) {

	UartSendString(SERIAL_PORT_PC, UartItoa((uint16_t)volumen, 10));
	UartSendString(SERIAL_PORT_PC, ' ');
	UartSendString(SERIAL_PORT_PC, " cm3 ");
	UartSendString(SERIAL_PORT_PC, "\r\n"); /*dejo un renglon*/
}


void HardConfig(void){

	SystemClockInit(); /*Inicializacioness*/
	SwitchesInit();
	UartInit(&mySerial);

	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3); /*Inicializo el driver de medicion de distacia*/
}

int main(void) {

	HardConfig();

	while (1) {

	}

	return 0;
}

/*==================[end of file]============================================*/

