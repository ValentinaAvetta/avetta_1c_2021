/*! @mainpage Recuperatorio Ejercicio 1 -Valentina Avetta
 *
 * \section genDesc General Description
 *
 * Implementacion de firmware de un sistema de medición de volumen, a partir de un sensor de distancia.
 * Se envia el dato 6 veces por segundo por la UART a la PC en el siguiente formato: xx + “ cm3” (uno por renglón).
 * Se utiliza interrupciones de timer. (Para el cálculo de volumen aproxime pi a 3,14).
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   	HCSR04		|	T_COL0		|
 * |  				|				|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Document creation		                         |
 * | 18/06/2021 | Document finished	                             |
 *
 *
 **
 * @author Valentina Avetta
 *
 */

#ifndef _RecuperatorioAvetta2
#define _RecuperatorioAvetta2


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
/** @fn void CalulaVolumen(void)
 * @brief Calcula el volumen del vaso
 * @param[in] ninguno
 * @return nada
 */
void CalulaVolumen(void);

/** @fn void HardConfig(void)
 * @brief Muestra en pantalla de PC el volumen del vaso cada 6 segundos
 * @param[in] ninguno
 * @return nada
 */
void DisplayPC(void);

/** @fn void HardConfig(void)
 * @brief Inicializo drivers
 * @param[in] ninguno
 * @return nada
 */
void HardConfig(void);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _RECUPERATORIOAVETTA1.H */

