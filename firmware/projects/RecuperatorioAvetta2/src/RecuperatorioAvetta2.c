/*! @mainpage Recuperatorio Ejercicio 2 -Valentina Avetta
 *
 * \section genDesc General Description
 *
 * El programa digitaliza una señal de ECG conectado a la entrada CH1 de la EDU-CIAA a una frecuencia de muestreo de 500 Hz.
 * Detecta QRS por umbral y envíe la cadena “QRS detectado” por el puerto serie a la PC cada vez que se encuentra un QRS.
 * El valor del umbral debe ser configurable tanto por comandos series como por las teclas.
 * Se usa la Tecla 1 y el carácter ‘U’ para subir el umbral en pasos de 2 [mV];
 * la Tecla 2 y el carácter ‘D’ disminuye el umbral en pasos de 2 [mV].
 * El rango completo para la variable va de 600 a 800.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   				|	CH1			|
 * |  				|				|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Document creation		                         |
 * | 18/06/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/RecuperatorioAvetta2.h"       /* <= own header */
#include "gpio.h"
#include "delay.h"
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void HardConfig(void);
void Sampling(void);
void DetectoQRS(void);
void doPort(void);

/*==================[external data definition]===============================*/

timer_config myTimer = {TIMER_A, 2, &Sampling};   /*Periodo de 2 ms, porque fm= 500 --> 1000/fm= 2*/
analog_input_config myAnalog= {CH1, AINPUTS_SINGLE_READ, &DetectoQRS};
serial_config mySerial= {SERIAL_PORT_PC, 115200, &doPort};

uint8_t umbral= 700; /*Umbral que detecta el QRS, lo inicializo a la mitad de los valores posibles que puedo seleccionar como umbral*/
uint8_t teclas; /*Variable que uso para seleccionar las teclas de la edu ciaa*/
uint8_t datoSerie; /*Variable que uso para subir y bajar el umbral por dato serie*/

float valorSenial;
float valorSenial_anterior=0;

/*==================[external functions definition]==========================*/

void Sampling(void){

	AnalogStartConvertion(); /*Comienzo la conversion AD*/
}

void DetectoQRS(void){

	AnalogInputRead(CH1, &entrada);

	valorSenial= (entrada*3.3/ 1024.0); /*Supongo que esta conectada a 3.3 V*/

	if (valorSenial > umbral && valorSenial_anterior< umbral) {

		if(valorSenial< umbral && valorSenial_anterior> umbral){

			UartSendString(SERIAL_PORT_PC, " QRS Detectado "); /*mando a PC que se detecto el QRS*/
			UartSendString(SERIAL_PORT_PC, "\r\n"); /*dejo un renglon*/
		}
	}

	valorSenial_anterior = valorSenial;
}

void doPort(void) {

	UartReadByte(SERIAL_PORT_PC, &datoSerie);

	switch (datoSerie) {

	case 'D': /*bajo*/
		if (umbral > 602) { /*mi tope minimo es 600 --> 600+2= 602*/
			umbral= umbral - 2; /*Puedo variar de a 2 mV*/
		}
		break;

	case 'U': /*subo*/
		if (umbral < 798) { /*mi tope maximo es 800--> 800-2= 798*/
			umbral = umbral + 2; /*Puedo variar de a 2 mV*/
		}
		break;
	}

}

void HardConfig(void) {

	SystemClockInit();
	SwitchesInit();
	UartInit(&mySerial);
	AnalogInputInit(&myAnalog);

	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);

}

int main(void) {

	HardConfig();

	while (1) {

		teclas = SwitchesRead();

		switch (teclas) {

		case SWITCH_1:
			if (umbral > 602) { /*mi tope minimo es 600 --> 600+2= 602*/
				umbral= umbral - 2; /*Puedo variar de a 2 mV*/
					}
			break;

		case SWITCH_2:
			if (umbral < 798) { /*mi tope maximo es 800--> 800-2= 798*/
				umbral = umbral + 2; /*Puedo variar de a 2 mV*/
			}
			break;
		}

	}

	return 0;
}

/*==================[end of file]============================================*/

