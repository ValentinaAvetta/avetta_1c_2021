/*! @mainpage Recuperatorio Ejercicio 2 -Valentina Avetta
 *
 * \section genDesc General Description
 * El programa digitaliza una señal de ECG conectado a la entrada CH1 de la EDU-CIAA a una frecuencia de muestreo de 500 Hz.
 * Detecta QRS por umbral y envíe la cadena “QRS detectado” por el puerto serie a la PC cada vez que se encuentra un QRS.
 * El valor del umbral debe ser configurable tanto por comandos series como por las teclas.
 * Se usa la Tecla 1 y el carácter ‘U’ para subir el umbral en pasos de 2 [mV];
 * la Tecla 2 y el carácter ‘D’ disminuye el umbral en pasos de 2 [mV].
 * El rango completo para la variable va de 600 a 800.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   				|	CH1			|
 * |  				|				|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 18/06/2021 | Document creation		                         |
 * | 18/06/2021 | Document finished	                             |
 *
 *
 **
 * @author Valentina Avetta
 *
 */

#ifndef _RecuperatorioAvetta2
#define _RecuperatorioAvetta2


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void Sampling(void)
 * @brief Muestreo la senial analogica del ECG
 * @param[in] ninguno
 * @return nada
 */
void Sampling(void);

/** @fn void DetectoQRS(void)
 * @brief Detecto QRS por umbral
 * @param[in] ninguno
 * @return nada
 */
void DetectoQRS(void);

/** @fn void doPort(void)
 * @brief Uso el teclado de la computadora para subir y bajar el umbral de deteccion
 * @param[in] ninguno
 * @return nada
 */
void doPort(void);

/** @fn void HardConfig(void)
 * @brief Inicializo drivers
 * @param[in] ninguno
 * @return nada
 */
void HardConfig(void);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _RECUPERATORIOAVETTA2.H */

