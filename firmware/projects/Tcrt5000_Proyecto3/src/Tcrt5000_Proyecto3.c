/*! @mainpage Tcrt5000
 *
 * \section genDesc General Description
 *
 * Project 3.
 * This application detects and counts lines (or objects) by an IR led sensor on EDU-CIAA
 * Also shows the amount of objects detected (from 1 to 15) on real time using four colors LEDS, and it shows
 * on a computer screen by the syntax ¨N lines¨.
 * It has use 3 computer keys with different functionalities (show amount of objects detected, hold and reset)
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	|GPIO_T_COL0	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 08/05/2021 | Document creation		                         |
 * | 10/05/2021 | Document editing		                         |
 * | 13/05/2021 | Document editing		                         |
 * | 14/05/2021 | Document editing		                         |
 * | 19/05/2021 | Document editing		                         |
 * | 21/05/2021 | Document finished	                         |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Tcrt5000_Proyecto3.h"       /* <= own header */

#include "gpio.h"
#include "led.h"
#include "tcrt5000.h"
#include "switch.h"
#include "delay.h"
#include "systemclock.h"

#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

gpio_t port= GPIO_T_COL0; /*Es el puerto donde conecto el perisferico*/
uint8_t binaryCount[4];   /*Binario de 4 bits*/
bool state = 0; /*Varible inicializada en cero para usar como bandera que permite el conteo*/
bool hold = 0; /*Varible inicializada en cero para usar como bandera en la funcion hold de la tecla 2*/

uint8_t count = 0; /*Varible para contar los objetos */
uint8_t countBin = 0; /*Varible para contar los objetos */

uint8_t estado_actual = 0;
uint8_t estado_anterior = 0;


/** @fn void ledsOff(void)
 * @brief Apaga todos los leds
 * @param[in] ninguno
 * @return nada
 */
void ledsOff(void){
	LedOff(LED_RGB_B);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
}

/** @fn ledOnBinary(void)
 * @brief Enciende los leds que representa los bits
 * @param[in] ninguno
 * @return nada
 */
void ledOnBinary(void){

	if (countBin & (1<<0)) //bit 0
		LedOn(LED_3);
	if (countBin & (1<<1)) //bit 1
		LedOn(LED_2);
	if (countBin & (1<<2)) //bit 2
		LedOn(LED_1);
	if (countBin & (1<<3)) //bit 3
		LedOn(LED_RGB_B);
}

/** @fn void doTcrt5000Read(void)
 * @brief Lee el estado del sensor IR y detecta si paso o no un objeto
 * @param[in] ninguno
 * @return nada
 */
void doTcrt5000Read(void) {

	estado_actual = Tcrt5000State();
	if ((estado_actual == 1) && (estado_anterior == 0)) {
		DelayMs(100);
		if (estado_actual == Tcrt5000State()) {
			count++;
			countBin++;
		}
	}
	estado_anterior = estado_actual;
}

/** @fn void myDispley (void)
 * @brief Me muestra el resultado de la cantidad de objetos detectados (en tiempo real) por medio de 4 leds
 * y me muestra en la pantalla de la computadora la cantidad de objetos
 * @param[in] ninguno
 * @return nada
 */
void myDispley(void) {

	ledsOff();
	ledOnBinary();

	UartSendString(SERIAL_PORT_PC, UartItoa(count, 10));
	UartSendString(SERIAL_PORT_PC, ' ');
	UartSendString(SERIAL_PORT_PC, " lineas ");
	UartSendString(SERIAL_PORT_PC, "\r\n");
}

/** @fn doPort()
 * @brief Lee las teclas del teclado de la computadora y realiza distintas funciones segun sea o, h u 0
 * @param[in] ninguno
 * @return nada
 */
void doPort(void) {

	uint8_t teclado;
	uint8_t stateOnOff = 1; /*Variable que uso como contador que luego utilizo para detectar si es par o no la presion de la tecla 1*/

	UartReadByte(SERIAL_PORT_PC, &teclado);

	switch (teclado) {

	case 'o':
		stateOnOff++;
		if (stateOnOff % 2 == 0) { /*Veo si es par o no. Si es par me cuenta, si es impar (volvio a presionar la tecla) no me cuenta*/
			state = 1;
		} else {
			state = 0;
		}
		break;

	case 'h':
		hold = 1;
		break;

	case '0':
		count = 0;
		countBin=0;
		break;
	}

}

/*==================[external functions definition]==========================*/

int main(void) {

	timer_config myTimer = {TIMER_A, 1000, &myDispley};
	serial_config myConfig= {SERIAL_PORT_PC, 115200, &doPort};

	SystemClockInit(); /*Inicializo para utilizar la funcion doTcrt5000Read*/
	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);

	Tcrt5000Init(port); /*Inicializo el puerto donde esta conectado el sensor perisferico*/
	LedsInit(); /*Inicializo LEDs con el driver de LEDS */

	UartInit(&myConfig); /*Inicializo la uart*/

	while (1) {

		if ((state == 1) && (hold == 0)) { /*Este bloque lo uso para la funcion de la tecla 1 y 2*/
			doTcrt5000Read();
		}

		if ((state == 1) && (hold == 1)) { /*Este bloque lo uso para la funcion de la tecla 1 y 2 CUANDO TENGO HOLD ACTIVO*/
			hold = 0;
			doTcrt5000Read(); /*Me cuenta pero no enciende las leds*/
		}
	}

	return 0;
}

/*==================[end of file]============================================*/

