/*! @mainpage Tcrt5000_Proyecto
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Valentina Avetta
 *
 */

#ifndef _TCRT5000_PROYECTO
#define _TCRT5000_PROYECTO


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void ledsOff(void)
 * @brief Apaga todos los leds
 * @param[in] ninguno
 * @return nada
 */
void ledsOff(void);

/** @fn ledOnBinary(void)
 * @brief Enciende los leds que representa los bits
 * @param[in] ninguno
 * @return nada
 */
void ledOnBinary(void);

/** @fn void doTcrt5000Read(void)
 * @brief Lee el estado del sensor IR y detecta si paso o no un objeto
 * @param[in] ninguno
 * @return nada
 */
void doTcrt5000Read(void);

/** @fn void myDispley (void)
 * @brief Me muestra el resultado de la cantidad de objetos detectados (en tiempo real) por medio de 4 leds
 * y me muestra en la pantalla de la computadora la cantidad de objetos
 * @param[in] ninguno
 * @return nada
 */
void myDispley(void);

/** @fn doPort()
 * @brief Lee las teclas del teclado de la computadora y realiza distintas funciones segun sea o, h u 0
 * @param[in] ninguno
 * @return nada
 */
void doPort(void);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _TCRT5000_H */

