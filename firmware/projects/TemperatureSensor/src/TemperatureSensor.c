/*! @mainpage Temperature Sensor
 *
 * \section genDesc General Description
 *
 * Sensor de temperatura que indica el estado de alerta de 4 valores de temperatura (maxima, minima, submaxima y subminima) a partir de LEDs de
 * distintos colores. Cuando se llega a la temperatura maximas o minima se dispara una alarma sonora, la cual se puede setear para que este activa
 * o desactiva. El sistema se puede reiniciar cuando se llega a alguna a las temperaturas extremas y se indica a que hora sucedio el hecho. Para
 * configurar la hora por Real Time se emplea puerto Serie.
 * Los valores de temperatura se promedian cada 10 muestras y se muestran por pantalla de PC cada 1 segundo.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   mcp9700		|	CH1			|
 * | active buzzer 	|	T-COL0		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 04/06/2021 | Document creation		                         |
 * | 11/06/2021 | Document editing		                         |
 * | 12/06/2021 | Document editing		                         |
 * | 13/06/2021 | Document editing		                         |
 * | 14/06/2021 | Document editing		                         |
 * | 15/06/2021 | Document editing		                         |
 * | 19/06/2021 | Document editing		                         |
 * | 20/06/2021 | Document editing		                         |
 * | 21/06/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/TemperatureSensor.h"       /* <= own header */
#include "gpio.h"
#include "delay.h"
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "sapi_rtc.h" /*Driver de Real Time Clock*/

/*==================[macros and definitions]=================================*/

#define MAXTEMP 37
#define SUB_MAXTEMP 35
#define MINTEMP 21
#define SUB_MINTEMP 25

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void Sampling(void);
void ReadTemperature(void);
void Alerts(void);
void AlertTime (void);
void Sound (void);
void HardConfig(void);
void ShowPromTemp (void);

/*==================[external data definition]===============================*/

timer_config myTimerA = {TIMER_A, 100, &Sampling};        /*Tomo temperatura cada 0.1 segundo*/
analog_input_config myAnalog= {CH1, AINPUTS_SINGLE_READ, &ReadTemperature};
serial_config mySerial= {SERIAL_PORT_PC, 115200, NULL};

uint16_t sample=0; /*Muestras tomadas del sensor */
uint16_t input=0; /*Valor de entrada analogica*/
float temperature=0; /*temperatura promedio de 10 muestras*/
float temperature10=0; /*suma de temperatura de 10 muestras*/
float voltaje=0;

bool alert=0; /*Alerta de llegada a temperatura extrema*/
bool alarmaSi=0; /*Alarma sonora*/
uint8_t stateOnOff=0; /*Para activar o desactivar la alarma sonora*/
uint8_t count=0; /*Contador de 10 muestras de temperatura*/

rtc_t FechaYHora; /*Fecha y hora en la que se detecta el exceso de temperatura*/
uint8_t datoSerie; /*Variable que modifico con el teclado de la PC para configurar la hora*/
uint8_t teclas; /*Tecla switch para reiniciar el sistema*/

/*==================[external functions definition]==========================*/

void Sampling(void){

	AnalogStartConvertion();
}

void ReadTemperature(void) {

		AnalogInputRead(CH1, &input);

		voltaje = (input * 3.3 / 1024.0) - 0.5; /* De la hoja de datos del mcp9700, T0C= 500 mV y Tc= 10 mV */
		temperature = voltaje / 0.01; /*La formula es ---> Tambiente = (Vout - V0C) / Tc */

		temperature10 = temperature10 + temperature; /*Acumulo 10 muestras para luego hacer el promedio*/
		count++;

		if (count == 10) {
			ShowPromTemp();
			count = 0;
		}
}

void ShowPromTemp (void){

	temperature= temperature10/ 10; /*Hago el promedio de 10 muestras*/
	Alerts();
	Sound();

	UartSendString(SERIAL_PORT_PC, UartItoa((uint32_t)temperature, 10)); /*Muestro en pantalla el valor de temperatura*/
	UartSendString(SERIAL_PORT_PC, " grad. C ");
	UartSendString(SERIAL_PORT_PC, "\r\n");

	temperature10=0;
	GPIOOff(GPIO_T_COL0);
}

void Sound(void) {

	if (alert == 1 && alarmaSi==1) {
		GPIOInit(GPIO_T_COL0, GPIO_OUTPUT);
		GPIOOn(GPIO_T_COL0);
		DelayMs(300);
		GPIOOff(GPIO_T_COL0);
	}
}

void AlertTime(void) {

	UartSendString(SERIAL_PORT_PC, "Se llego a la Temp. no deseada a las:  ");
	UartSendString(SERIAL_PORT_PC, UartItoa(FechaYHora.hour, 10));
	UartSendString(SERIAL_PORT_PC, " : ");
	UartSendString(SERIAL_PORT_PC, UartItoa(FechaYHora.min, 10));
	UartSendString(SERIAL_PORT_PC, " : ");
	UartSendString(SERIAL_PORT_PC, UartItoa(FechaYHora.sec, 10));
	UartSendString(SERIAL_PORT_PC, "\r\n");
	DelayMs(200);
}

void Alerts(void) {

	if (temperature < MINTEMP) {
		LedOff(LED_2);  /* Apago el led rojo */
		LedOn(LED_RGB_B);

		rtcRead(&FechaYHora); /*Leo la hora que llega a la alerta*/

		alert=1;
	}

	if (temperature > MINTEMP && temperature < SUB_MINTEMP && alert==0) {
		LedOff(LED_2);  /* Apago el led rojo */
		LedOn(LED_RGB_B);
		DelayMs(1000);
		LedOff(LED_RGB_B);
		DelayMs(100);
	}

	if (temperature > MAXTEMP) {
		LedOff(LED_RGB_B); /* Apago el led azul */
		LedOn(LED_2);

		rtcRead(&FechaYHora); /*Leo la hora que llega a la alerta*/

		alert=1;
	}

	if (temperature < MAXTEMP && temperature > SUB_MAXTEMP && alert==0) {
		LedOff(LED_RGB_B); /* Apago el led azul */
		LedOn(LED_2);
		DelayMs(100);
		LedOff(LED_2);
		DelayMs(100);
	}
}

void HardConfig(void){

	SystemClockInit();
	UartInit(&mySerial);
	LedsInit();
	SwitchesInit();
	AnalogInputInit(&myAnalog);
	rtcInit();

	TimerInit(&myTimerA);
	TimerStart(TIMER_A);
}

int main(void) {

	HardConfig();
	LedOn(LED_3); /*Me informa que el sistema esta andando*/
	TimerStop(TIMER_A);

	UartSendString(SERIAL_PORT_PC,
			"Sistema inicializado. Configure la hora precionando la letra 'c' ");
	UartSendString(SERIAL_PORT_PC, "\r\n");
	UartSendString(SERIAL_PORT_PC, "\r\n");

	while (1) {

		UartReadByte(SERIAL_PORT_PC, &datoSerie);
		if (datoSerie == 'c') { /*apreto c para configurar la hora */
			do {
				UartReadByte(SERIAL_PORT_PC, &datoSerie);
			} while (datoSerie != 13);   //esto es para limpiar el buffer serie

			UartSendString(SERIAL_PORT_PC, "Ingrese la hora: ");
			FechaYHora.hour = UartReadNumber(SERIAL_PORT_PC);
			rtcWrite(&FechaYHora);
			UartSendString(SERIAL_PORT_PC, UartItoa(FechaYHora.hour, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");

			do {
				UartReadByte(SERIAL_PORT_PC, &datoSerie);
			} while (datoSerie != 13);   //esto es para limpiar el buffer serie

			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos: ");
			FechaYHora.min = UartReadNumber(SERIAL_PORT_PC);
			rtcWrite(&FechaYHora);
			UartSendString(SERIAL_PORT_PC, UartItoa(FechaYHora.min, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");

			do {
				UartReadByte(SERIAL_PORT_PC, &datoSerie);
			} while (datoSerie != 13);   //esto es para limpiar el buffer serie

			UartSendString(SERIAL_PORT_PC, "Ingrese los segundos: ");
			FechaYHora.sec = UartReadNumber(SERIAL_PORT_PC);
			rtcWrite(&FechaYHora);
			UartSendString(SERIAL_PORT_PC, UartItoa(FechaYHora.sec, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			UartSendString(SERIAL_PORT_PC, "\r\n");

			UartSendString(SERIAL_PORT_PC, "Si desea que el sistema funcione con alarma sonora presione la tecla 1");
			UartSendString(SERIAL_PORT_PC, "\r\n");
			UartSendString(SERIAL_PORT_PC, "Para reiniciar el sistema presione la tecla 2");
			UartSendString(SERIAL_PORT_PC, "\r\n");
			UartSendString(SERIAL_PORT_PC, "\r\n");

			TimerStart(TIMER_A);
		}

		teclas = SwitchesRead();
		switch (teclas) {

		case SWITCH_1: /*Tecla para setear alarma sonora*/
			stateOnOff++;
			if (stateOnOff % 2 == 0) { /*Veo si es par o no. Si es par me cuenta, si es impar (volvio a presionar la tecla) no me cuenta*/
				alarmaSi = 1;
			} else {
				alarmaSi = 1;
			}
			break;

		case SWITCH_2: /*Tecla de reseteo*/
			LedOff(LED_2); /* Apago el led rojo */
			LedOff(LED_RGB_B); /* Apago el led azul */

			if (alert == 1) { /*Pongo el if por si se resetea sin que haya llegado a la temperatura extrema */
				AlertTime();
			}

			alert = 0;
			break;
		}
	}

	return 0;
}

/*==================[end of file]============================================*/

