/*! @mainpage Temperature Sensor
 *
 * \section genDesc General Description
 *
 * Sensor de temperatura que indica el estado de alerta de 4 valores de temperatura (maxima, minima, submaxima y subminima) a partir de LEDs de
 * distintos colores. Cuando se llega a la temperatura maximas o minima se dispara una alarma sonora, la cual se puede setear para que este activa
 * o desactiva. El sistema se puede reiniciar cuando se llega a alguna a las temperaturas extremas y se indica a que hora sucedio el hecho. Para
 * configurar la hora por Real Time se emplea puerto Serie.
 * Los valores de temperatura se promedian cada 10 muestras y se muestran por pantalla de PC cada 1 segundo.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |   mcp9700		|	CH1			|
 * | active buzzer 	|	T-COL0		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 04/06/2021 | Document creation		                         |
 * | 11/06/2021 | Document editing		                         |
 * | 12/06/2021 | Document editing		                         |
 * | 13/06/2021 | Document editing		                         |
 * | 14/06/2021 | Document editing		                         |
 * | 15/06/2021 | Document editing		                         |
 * | 19/06/2021 | Document editing		                         |
 * | 20/06/2021 | Document editing		                         |
 * | 21/06/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

#ifndef _SensorTemperature
#define _SensorTemperature


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void Sampling(void)
 * @brief Toma las muestras de temperatura cada 0.1 seg a partir de la funcion AnalogStartConvertion();
 * @param[in] ninguno
 * @return nada
 */
void Sampling(void);

/** @fn void ReadTemperature(void)
 * @brief Leo la entrada analogica del sensor de temperatura y hago la conversion a grados centigrados. Acumulo valores de 10 muestras para luego hacer el promedio
 * @param[in] ninguno
 * @return nada
 */
void ReadTemperature(void);

/** @fn void Alerts(void)
 * @brief Comparo valores de temperatura para generar alertas con alarma sonora y encendido de LEDs
 * @param[in] ninguno
 * @return nada
 */
void Alerts(void);

/** @fn void AlertTime (void)
 * @brief Guardo la fecha y hora de cuando se llega a los valores de temperatura definidos como extremos y los muestro en pantalla con puerto serie
 * @param[in] ninguno
 * @return nada
 */
void AlertTime (void);

/** @fn void Sound (void)
 * @brief Genero una alarma sonora con un active buzzer
 * @param[in] ninguno
 * @return nada
 */
void Sound (void);

/** @fn void HardConfig(void)
 * @brief Inicializo drivers
 * @param[in] ninguno
 * @return nada
 */
void HardConfig(void);

/** @fn void ShowPromTemp (void)
 * @brief Muestro por pantalla con puerto serie el promedio de temperatura de 10 muestras, cada 1 segundo.
 * @param[in] ninguno
 * @return nada
 */
void ShowPromTemp (void);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _TCRT5000_H */

