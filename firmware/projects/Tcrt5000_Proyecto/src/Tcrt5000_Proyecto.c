/*! @mainpage Tcrt5000
 *
 * \section genDesc General Description
 *
 * Project 2.
 * This application detects and counts lines (or objects) by an IR led sensor on EDU-CIAA
 * Also show the amount of objects detected (from 1 to 15) on real time using leds of four colors.
 * It has use 3 switch with different functionalities (show amount of objects detected, hold and reset)
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	|GPIO_T_COL0	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/04/2021 | Document creation		                         |
 * | 28/04/2021 | Document continue 					         |
 * | 5/05/2021  | Document continue
 * | 7/05/2021  | Document continue
 * | 8/05/2021  | Document finished
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Tcrt5000_Proyecto.h"       /* <= own header */
#include "gpio.h"
#include "led.h"
#include "tcrt5000.h"
#include "switch.h"
#include "delay.h"
#include "systemclock.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

gpio_t port= GPIO_T_COL0; /*Es el puerto donde conecto el perisferico*/
uint8_t binaryCount[4];   /*Binario de 4 bits*/

void contToBinary (uint8_t contador) {

	uint8_t i = 4;

	while (i>0){   /*Solo puede contar de 0 a 15 porque solo tengo 4 bits (4 leds))*/
		i --;
		binaryCount[i] = contador%2;
		contador= contador/2;
	}
}
/** @fn void contToBinary (uint8_t contador)
 * @brief Convierte de decimal (cnumero de conteos de objetos) a binario. Solo puede contar de 0 a 15 (4 bits)
 * @param[in] contador
 * @return nada
 */

void ledsOff(void){
	LedOff(LED_RGB_B);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
}
/** @fn void ledsOff()
 * @brief Apaga todos los leds
 * @param[in] ninguno
 * @return nada
 */

void ledOnBinary(void){

	if (binaryCount[3] ==1) //bit 3
		LedOn(LED_3);
	if (binaryCount[2] ==1) //bit 2
		LedOn(LED_2);
	if (binaryCount[1] ==1) //bit 1
		LedOn(LED_1);
	if (binaryCount[0] ==1) //bit 0
		LedOn(LED_RGB_B);
}
/** @fn ledOnBinary()
 * @brief Enciende los leds que representa los bits (mi "pantalla")
 * @param[in] ninguno
 * @return nada
 */

/*==================[external functions definition]==========================*/

int main(void) {

	Tcrt5000Init(port); /*Inicializo el puerto donde esta conectado el sensor perisferico*/
	LedsInit(); /*Inicializo LEDs con el driver de LEDS */
	uint8_t count = 0; /*Varible para contar los objetos */
	SystemClockInit(); /*Inicializo para utilizar la funcion DelayMs del driver Delay*/

	SwitchesInit(); /*Inicializo los switches*/
	uint8_t teclas; /*Para usar los switches*/
	bool hold = 0; /*Varible inicializada en cero para usar como bandera en la funcion hold de la tecla 2*/
	bool state = 0; /*Varible inicializada en cero para usar como bandera que permite el conteo*/
	uint8_t stateOnOff = 1; /*Variable que uso como contador que luego utilizo para detectar si es par o no la presion de la tecla 1*/

	while (1) {

		teclas = SwitchesRead();

		switch (teclas) {

		case SWITCH_1:
			stateOnOff++;
			if (stateOnOff %2 == 0){ /*Veo si es par o no. Si es par me cuenta, si es impar (volvio a presionar la tecla) no me cuenta*/
				state = 1;}
			else{
				state = 0;}
			DelayMs(200);   /*Espero 200 seg*/
			break;

		case SWITCH_2:
			hold = 1;     /*Se debe mantener presionado durante el tiempo que se desee el hold*/
			break;

		case SWITCH_3:
			count = 0;   /*Funcion de reseteo del conteo de objetos*/
			break;
		}

		if ((state == 1) && (hold == 0)) { /*Este bloque lo uso para la funcion de la tecla 1 y 2*/
			if (Tcrt5000State() == 1) {
				if (Tcrt5000DownEdge() == 1) {
					count++;
					contToBinary(count);
					ledsOff();
					ledOnBinary();
				}
			}
		}

		if ((state == 1) && (hold == 1)) { /*Este bloque lo uso para la funcion de la tecla 1 y 2 CUANDO TENGO HOLD ACTIVO*/
			hold=0;
			if (Tcrt5000State() == 1) {
				if (Tcrt5000DownEdge() == 1) {
					count++;                      /*Me cuenta pero no me enciende las leds*/
				}
			}
		}
	}

	return 0;
}

/*==================[end of file]============================================*/

