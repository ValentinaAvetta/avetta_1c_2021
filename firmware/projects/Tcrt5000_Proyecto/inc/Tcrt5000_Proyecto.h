/*! @mainpage Tcrt5000_Proyecto
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Valentina Avetta
 *
 */

#ifndef _TCRT5000_PROYECTO
#define _TCRT5000_PROYECTO


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void contToBinary (uint8_t contador)
 * @brief Convierte de decimal (cnumero de conteos de objetos) a binario. Solo puede contar de 0 a 15 (4 bits)
 * @param[in] contador
 * @return nada
 */
void contToBinary (uint8_t contador);

/** @fn void ledsOff()
 * @brief Apaga todos los leds
 * @param[in] ninguno
 * @return nada
 */
void ledsOff(void);

/** @fn ledOnBinary()
 * @brief Enciende los leds que representa los bits (mi "pantalla")
 * @param[in] ninguno
 * @return nada
 */
void ledOnBinary(void);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/


#endif /* #ifndef _TCRT5000_H */

