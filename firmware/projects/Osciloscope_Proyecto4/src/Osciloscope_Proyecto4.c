/*! @mainpage Osciloscope
 *
 * \section genDesc General Description
 *
 * Project 4
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		 	|	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * | --/--/2021 | Document editing		                         |
 * | --/--/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Osciloscope_Proyecto4.h"       /* <= own header */

#include "gpio.h"
#include "delay.h"
#include "systemclock.h"

#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

serial_config mySerial= {SERIAL_PORT_PC, 115200, NULL};
uint16_t dato;

void InterrupcionTimer(void){

	AnalogStartConvertion();
	//AnalogInputReadPolling(CH1, &dato);

}
void leerDato(void){

	AnalogInputRead(CH1,&dato);
	UartSendString(SERIAL_PORT_PC, UartItoa(dato, 10));
    UartSendString(SERIAL_PORT_PC, "\r");

}

/*==================[external functions definition]==========================*/

int main(void) {

	timer_config myTimer = {TIMER_A, 2, &InterrupcionTimer};
	analog_input_config myAnalog= {CH1, AINPUTS_SINGLE_READ, &leerDato};

	SystemClockInit();
	UartInit(&mySerial);
	AnalogInputInit(&myAnalog);

	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);

	while (1) {


	}

	return 0;
}

/*==================[end of file]============================================*/

