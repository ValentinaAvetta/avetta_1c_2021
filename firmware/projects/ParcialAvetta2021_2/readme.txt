﻿Descripción del Proyecto.

Ej 2 del Parcial 

Control de fase digital que controla la velocidad de un motor universal. Digitaliza senoidal de la red con 1 ms de resolucion 
y detecta el cruce por cero. Luego de R0 ms dispara un triac con un pulso de 1 ms. 

El valor de R0 toma valores ente 0 y 20 ms con pasos de 5 ms, esto es controlado a través de puerto serie con 
los comandos ‘S’ (subir) y ‘B’ (bajar).

Teniendo en cuenta que el CAD del LCP4337 (EDU-CIAA) es monopolar, se considera que la señal de entrada es 
acondicionada por el bloque obteniedno una excursión de 0 a 3.3 V y valor medio 1.65 V.