/*! @mainpage Parcial 1C 2021
 *
 * \section genDesc General Description
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 9/06/2021 | Document creation		                         |
 *
 **
 * @author Valentina Avetta
 *
 */

#ifndef _ParcialAvetta2021_2
#define _ParcialAvetta2021_2


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void Disparo(void)
 * @brief   Aun no esta implementada. Deberia generar el pulso de 1 ms
 * @param[in] No Parameter
 * @return nothing
 */
void Disparo(void);

/** @fn CrucePorCero(void)
 * @brief   Detecta el cruce por cero y lee la senial
 * @param[in] No Parameter
 * @return nothing
 */
void CrucePorCero(void);

/** @fn void Muestrear(void)
 * @brief   Muestre la senial Analogica
 * @param[in] No Parameter
 * @return nothing
 */
void Muestrear(void);

/** @fn doPort(void)
 * @brief   Lee las teblas que manejan las instrucciones dadas por las tecla del teclado de la pc para subir o vajar r0
 * @param[in] No Parameter
 * @return nothing
 */
void doPort(void);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIALAVETTA2021_2_H */

