/*! @mainpage Parcial 1C 2021- Ejercicio 2
 *
 * \section genDesc General Description
 *
 * Control de fase digital que controla la velocidad de un motor universal. Digitaliza senoidal de la red con 1 ms de resolucion
 * y detecta el cruce por cero. Luego de R0 ms dispara un triac con un pulso de 1 ms.
 *
 * El valor de R0 toma valores ente 0 y 20 ms con pasos de 5 ms, esto es controlado a través de puerto serie con
 * los comandos ‘S’ (subir) y ‘B’ (bajar).
 *
 * Teniendo en cuenta que el CAD del LCP4337 (EDU-CIAA) es monopolar, se considera que la señal de entrada es
 * acondicionada por el bloque obteniedno una excursión de 0 a 3.3 V y valor medio 1.65 V.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		 	|	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ParcialAvetta2021_2/inc/ParcialAvetta2021_2.h"       /* <= own header */

#include "gpio.h"
#include "delay.h"
#include "systemclock.h"
#include "switch.h"

#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
#define CERO 512 /*Valor medio= 1024/2= 512*/
/*==================[internal functions declaration]=========================*/

void CrucePorCero(void);

void Muestrear(void);

void doPort(void);

/*==================[external data definition]===============================*/

uint8_t r0;
uint8_t count;
uint16_t entrada=0;
bool disparo=0;
float valor;
bool bandera=0; /*detecto cruce por cero*/

timer_config myTimer = {TIMER_A, 1, &Muestrear}; /*1 ms*/
analog_input_config myAnalog= {CH1, AINPUTS_SINGLE_READ, &CrucePorCero};
serial_config mySerial= {SERIAL_PORT_PC, 115200, &doPort};

/*==================[external functions definition]==========================*/

void Muestrear(void){

	AnalogStartConvertion();
}

void CrucePorCero(void) {

	AnalogInputRead(CH1, &entrada);

	count++;
	valor = (entrada*3.3/ 1024.0);

	UartSendString(SERIAL_PORT_PC, UartItoa((uint32_t) valor, 10)); /*Digitaliza la senial*/
	UartSendString(SERIAL_PORT_PC, "\r");

	doPort(); /*Actualizo el valor de r0*/

	if ((valor == CERO) || (valor < CERO && valor_anterior > CERO)
			|| (valor > CERO && valor_anterior < CERO)) {

		LedOn(LED_RGB_B); /*Detecto el cruce por cero y lo evidencio con un LED*/
		badera=1;        /*Me indica que ocurrio un cruce por cero*/

	} else {
		LedOff(LED_RGB_B); /*Apago LED si no hay cruce por cero*/
	}

	if (bandera==1){
		if (count== r0){
			disparo=1;    /*Efectuo el disparo */
			count=0;
		}
	}

}

void doPort(void) {

	uint8_t datoSerie;
	UartReadByte(SERIAL_PORT_PC, &datoSerie);

	switch (teclado) {

	case 'b': /*bajo*/
		if (r0 > 5) { /*mi tope es 0*/
			r0= r0 - 5;
		}
		break;

	case 's': /*subo*/
		if (r0 < 15) { /*mi tope maximo es 20*/
			r0 = r0 + 5;
		}
		break;
	}

}

void HardConfig(void){

	SystemClockInit();      /*Inicializaciones de drivers*/
	UartInit(&mySerial);
	AnalogInputInit(&myAnalog);

	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);
}

int main(void) {

	while (1) {

	}

	return 0;
}

/*==================[end of file]============================================*/

