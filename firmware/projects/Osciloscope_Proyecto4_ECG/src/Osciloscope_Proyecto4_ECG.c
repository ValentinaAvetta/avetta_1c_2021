/*! @mainpage Osciloscope ECG
 *
 * \section genDesc General Description
 *
 * Project 4.
 * Diseñar e implementar una aplicación, basada en el driver analog_io.h y el driver de transmisión serie uart.h,
 * que digitalice una señal analogica y la transmita a un graficador de puerto serie de la PC.
 * Se debe tomar la entrada CH1 del conversor AD y la transmisión se debe realizar por la UART conectada al
 * puerto serie de la PC, en un formato compatible con un graficador por puerto serie.
 * Fm es de 500 Hz (2ms). La del ECG de 4ms.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		 	|	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * | 28/05/2021 | Document editing		                         |
 * | 28/05/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Osciloscope_Proyecto4_ECG.h"       /* <= own header */

#include "gpio.h"
#include "delay.h"
#include "systemclock.h"
#include "switch.h"

#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
#define BUFFER_SIZE 231
#define PI 3.141592

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

serial_config mySerial= {SERIAL_PORT_PC, 115200, NULL};
uint8_t i=0;

uint8_t fc=10;
float RC=0;
float alfa=0;
uint16_t entrada=0;
uint16_t salida_filtrada_anterior= 0;
uint16_t salida_filtrada= 0;

bool filter=0;

void InterrupcionTimer(void){

	AnalogStartConvertion();

}

void WriteECG (void){

	AnalogOutputWrite(ecg[i]);
	i++;
	if (i==BUFFER_SIZE){
		i=0;
	}

}

void CalculateAlfa (void){

	RC= 1/ (2* PI *fc);
	alfa= 0.002 / (RC + 0.002);

}

void ReadAndFilterECG(void) {                    //Filtro pasa bajo

	AnalogInputRead(CH1, &entrada);  /*leo el ecg y lo guardo en entrada*/

	if (filter == 1) {
		salida_filtrada = salida_filtrada_anterior
				+ alfa * (entrada - salida_filtrada_anterior);
		salida_filtrada_anterior = salida_filtrada;

		UartSendString(SERIAL_PORT_PC, UartItoa(salida_filtrada, 10));
		UartSendString(SERIAL_PORT_PC, "\r");
	}

	if (filter == 0){
		UartSendString(SERIAL_PORT_PC, UartItoa(entrada, 10));
		UartSendString(SERIAL_PORT_PC, "\r");
	}
	i++;
}


/*==================[external functions definition]==========================*/

int main(void) {

	timer_config myTimer = {TIMER_A, 2, &InterrupcionTimer};
	timer_config myTimerB = {TIMER_B, 4, &WriteECG};                   /*HABRIA QUE OPTIMIZAR ESTO. NO USAR OTRO TIMER*/
	analog_input_config myAnalog= {CH1, AINPUTS_SINGLE_READ, &ReadAndFilterECG};

	SystemClockInit();
	UartInit(&mySerial);
	AnalogInputInit(&myAnalog);
	AnalogOutputInit();

	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);

	TimerInit(&myTimerB); /*Inicializo el timer*/
	TimerStart(TIMER_B);

	SwitchesInit();

	while (1) {

		uint8_t teclas;
		teclas = SwitchesRead();

		CalculateAlfa();

		switch (teclas) {

		case SWITCH_1:     //activa el filtro
			filter=1;
			break;

		case SWITCH_2:  //desactiva el filtro
			filter=0;
			break;

		case SWITCH_3:   //baja frecuencia
			if(fc>5)
			fc= fc-5;
			CalculateAlfa();
			DelayMs(200);
			break;

		case SWITCH_4:  //sube frecuencia
			if(fc<80)  //le pongo un tope de 80
			fc= fc+5;
			CalculateAlfa();
			DelayMs(200);
			break;
		}

	}

	return 0;
}

/*==================[end of file]============================================*/

