/*! @mainpage Osciloscope ECG
 *
 * \section genDesc General Description
 *
 * Project 4.
 * Diseñar e implementar una aplicación, basada en el driver analog_io.h y el driver de transmisión serie uart.h,
 * que digitalice una señal analogica y la transmita a un graficador de puerto serie de la PC.
 * Se debe tomar la entrada CH1 del conversor AD y la transmisión se debe realizar por la UART conectada al
 * puerto serie de la PC, en un formato compatible con un graficador por puerto serie.
 * Fm es de 500 Hz (2ms). La del ECG de 4ms.
 *
 * La tecla 1 activa el filtro, la 2 lo desactiva. La tecla 3 baja la frecuencia de corte y la 4 la baja.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		 	|	|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 21/05/2021 | Document creation		                         |
 * | 28/05/2021 | Document editing		                         |
 * | 28/05/2021 | Document finished	                             |
 *
 **
 * @author Valentina Avetta
 *
 */

#ifndef _Osciloscope_Proyecto4_ECG
#define _Osciloscope_Proyecto4_ECG


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void InterrupcionTimer(void)
 * @brief   Use the function to use IRQ, read single channel (from analog_io driver)
 * @param[in] No Parameter
 * @return nothing
 */
void InterrupcionTimer(void);

/** @fn voidWriteECG(void)
 * @brief  Escribe la funcion de ECG
 * @param[in] No Parameter
 * @return nothing
 */
void WriteECG (void);

/** @fn void CalculateAlfa(void)
 * @brief   Calcula el valor de alfa de la funcion filtro
 * @param[in] No Parameter
 * @return nothing
 */
void CalculateAlfa (void);

/** @fn void ReadAndFilterECGr(void)
 * @brief   Aplica el filtro del ECG (o no, segun corresponda) y lo muestra en pantalla
 * @param[in] No Parameter
 * @return nothing
 */
void ReadAndFilterECG(void);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /* #ifndef _TCRT5000_H */

