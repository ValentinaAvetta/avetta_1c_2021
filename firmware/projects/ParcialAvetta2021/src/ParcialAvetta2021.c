/*! @mainpage Parcial 1C 2021 -Ejercicio 1
 *
 * \section genDesc General Description
 *
 * EJERCICIO:
 *
 * El sistema debe realizar mediciones de distancia a razón de 10 muestras por segundo, y utilizar dichas mediciones para calcular
 * la velocidad de los vehículos en m/s (la dirección de avance de los vehículos es siempre hacia el sensor).
 * Los valores de velocidad deben ser enviados a la PC a través del puerto serie, con el formato xx + “ m/s”.
 * Se debe además utilizar los LEDs de la EDU-CIAA como señales de advertencia de velocidad:
 * velocidad menor a 3m/s: LED3,
 * velocidad entre 3m/s y 8m/s: LED2,
 * velocidad mayor a 8m/s: LED1.
 * Finalmente, el sistema debe poder encenderse y apagarse usando la tecla 1 de la EDU-CIAA.
 * Indicar el estado encendido utilizando el LED RGB azul.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	HC-SR04 	|				|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 *
 **
 * @author Valentina Avetta
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ParcialAvetta2021.h"       /* <= own header */

#include "gpio.h"
#include "delay.h"
#include "systemclock.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "HCSR04.h"

/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/

void CalculaVelocidad(void);

void DisplayPC (void);

void DisplayBoard(void);

void HardConfig(void);

/*==================[internal data definition]===============================*/

timer_config myTimer = {TIMER_A, 100, &CalculaVelocidad}; /*Periodo de 100 ms, porque fm= 10 --> 1000/fm= 100 ms*/
serial_config mySerial= {SERIAL_PORT_PC, 115200, NULL};

float velocidad=0;
float distanciaAnterior=0;
float distancia=0;

uint8_t stateOnOff;
uint8_t teclado;
uint8_t state;

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

void CalculaVelocidad(void) {

	if (state == 1) {

		ditancia = HcSr04ReadDistanceCentimeters / 100; /*Paso de cm a m cuando lo divido por 100*/
		velocidad = (distancia - distanciaAnterior) / 0.1; /*Velocidad en m/s, como tomo muestras cada 100 ms --> divido por 0.1 seg*/
		distanciaAnterior = distancia;

		DisplayBoard();
		DisplayPC();
	}
}

void DisplayPC(void) {

	UartSendString(SERIAL_PORT_PC, UartItoa((uint16_t)velocidad, 10));
	UartSendString(SERIAL_PORT_PC, ' ');
	UartSendString(SERIAL_PORT_PC, " m/s ");
	UartSendString(SERIAL_PORT_PC, "\r\n");
}

void DisplayBoard(void) {

	if (velocidad < 3) {
		LedOff(LED_2);
		LedOff(LED_1);
		LedOn(LED_3);
	}

	if (velocidad < 3 && velocidad > 8) {
		LedOff(LED_3);
		LedOff(LED_1);
		LedOn(LED_2);
	}

	if (velocidad > 8) {
		LedOff(LED_2);
		LedOff(LED_3);
		LedOn(LED_1);
	}
}

void HardConfig(void){

	SystemClockInit(); /*Inicializaciones de drivers*/
	SwitchesInit();
	UartInit(&mySerial);
	LedsInit(); /*Inicializaciones de LEDs*/

	TimerInit(&myTimer); /*Inicializo el timer*/
	TimerStart(TIMER_A);
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3); /*Inicializo el driver*/
}

int main(void) {

	HardConfig();

	while (1) {

		teclado = SwitchesRead();

		switch (teclado) {

		case SWITCH_1: /*TECLA DE ENCEDIDO O APAGADO DEL SISTEMA*/
			stateOnOff++;
			if (stateOnOff % 2 == 0) { /*Veo si es par o no. Si es par me cuenta, si es impar (volvio a presionar la tecla) no me cuenta*/
				state = 1;
			} else {
				state = 0;
			}
			break;
		}

		if (state == 1) { /*LED que me indica si el sistema esta apagado o encendido*/
			LedOn(LED_RGB_B);
		} else {
			LedOff(LED_RGB_B);
		}

	}

	return 0;
}

/*==================[end of file]============================================*/

