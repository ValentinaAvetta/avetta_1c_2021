/*! @mainpage Parcial 1C 2021 -Ejercicio 1
 *
 * \section genDesc General Description
 *
 * EJERCICIO:
 *
 * El sistema debe realizar mediciones de distancia a razón de 10 muestras por segundo, y utilizar dichas mediciones para calcular
 * la velocidad de los vehículos en m/s (la dirección de avance de los vehículos es siempre hacia el sensor).
 * Los valores de velocidad deben ser enviados a la PC a través del puerto serie, con el formato xx + “ m/s”.
 * Se debe además utilizar los LEDs de la EDU-CIAA como señales de advertencia de velocidad:
 * velocidad menor a 3m/s: LED3,
 * velocidad entre 3m/s y 8m/s: LED2,
 * velocidad mayor a 8m/s: LED1.
 * Finalmente, el sistema debe poder encenderse y apagarse usando la tecla 1 de la EDU-CIAA.
 * Indicar el estado encendido utilizando el LED RGB azul.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	HC-SR04 	|				|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/06/2021 | Document creation		                         |
 *
 **
 * @author Valentina Avetta
 *
 */

#ifndef _ParcialAvetta2021
#define _ParcialAvetta2021


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @fn void CalculaVelocidad(void)
 * @brief   Calcula la velocidad en m/s
 * @param[in] No Parameter
 * @return nothing
 */
void CalculaVelocidad(void);

/** @fn void DisplayPC (void)
 * @brief   Muestra la velocidad en m/s por pantalla en PC a traves de puerto serie
 * @param[in] No Parameter
 * @return nothing
 */
void DisplayPC (void);

/** @fn void DisplayBoard(void)
 * @brief  Enciende y apaga LEDs de distinto color dependiendo del valor de la velocidad
 * @param[in] No Parameter
 * @return nothing
 */
void DisplayBoard(void);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIALAVETTA2021_H */

