﻿Descripción del Proyecto.

Ej 1 del Parcial 

Sistema que realiza mediciones de distancia a razon de 10 muestras por segundo. Calula la velocidad de m/s, y dichos valores  
son enviados a la PC a traves de puerto serie. Ademas emplea LEDs como señales de advertencia de velocidad:
- Menor a 3 m/s (LED 3)
- Entre 3 y 8 m/s (LED 2)
- Mayor a 8 m/s (LED 1)

El sistema se enciende y apaga usando la tecla 1. El estado de encendido se indica con LED RGB azul. 
