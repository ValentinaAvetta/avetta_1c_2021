
/** \brief Tcrt5000 driver in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 * Valentina Avetta
 */


/*==================[inclusions]=============================================*/
#include "tcrt5000.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
gpio_t  gpio_dout;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool Tcrt5000Init(gpio_t dout){           /*Conecta el dispositivo*/

	gpio_dout= dout;
	GPIOInit(dout, GPIO_INPUT);

	return 1;
}

bool Tcrt5000State(void){                /*Indica si el sensor esta leyendo la senial*/

	if (GPIORead(gpio_dout) == 1)
		return 1;
	else
		return 0;
}
bool Tcrt5000Deinit(gpio_t dout);       /*Desconecta el dispositivo*/


bool Tcrt5000DownEdge(void){    			 /*Detecta flanco descendente, lo uso para poder contar los objetos*/

	if (Tcrt5000State()==1){
		DelayTcrt5000();
	}
	if (Tcrt5000State()==0){
		return 1;
	}
}

void DelayTcrt5000(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}


/*==================[end of file]============================================*/
