
/** \brief HcSr04 driver in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 * Valentina Avetta
 */


/*==================[inclusions]=============================================*/
#include "HCSR04.h"
#include "gpio.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/
gpio_t puerto_de_echo;
gpio_t puerto_de_trigger;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger) {

	puerto_de_echo = echo;
	puerto_de_trigger = trigger;

	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);

	return 1;

}

uint16_t HcSr04ReadDistanceCentimeters(void) {

	GPIOOn(puerto_de_trigger);
	DelayUs(10);
	GPIOOff(puerto_de_trigger);

	uint16_t distancia_cm = 0;

	while (GPIORead(puerto_de_echo) == 0) {
	}

	while (GPIORead(puerto_de_echo) == 1) {
		DelayUs(1);
		distancia_cm++;
	}

	distancia_cm = distancia_cm / 28;
	return distancia_cm;
}
uint16_t HcSr04ReadDistanceInches(void) {

	GPIOOn(puerto_de_trigger);
	DelayMs(10);
	GPIOOff(puerto_de_trigger);

	uint16_t distancia_in = 0;

	while (GPIORead(puerto_de_echo) == 0) {
	}

	while (GPIORead(puerto_de_echo) == 1) {
		DelayUs(1);
		distancia_in++;
	}

	distancia_in = distancia_in / 60;
	return distancia_in;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger) {

	return 1;
}


/*==================[end of file]============================================*/
