/*
 * Valentina Avetta
 */
#ifndef TCRT5000_H
#define TCRT5000_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Tcrt5000
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 * This is a driver for six leds mounted on the board
 **
 **/


/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"

/*==================[macros]=================================================*/
#define lpc4337            1
#define mk60fx512vlq15     2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

bool Tcrt5000Init(gpio_t dout);

/** @fn bool Tcrt5000Init(gpio_t dout)
 * @brief Inicializa el perisferico Tcrt
 * @param[in] gpio_t
 * @return FALSE if an error occurs, in other case returns TRUE
 */

bool Tcrt5000State(void);

/** @fn bool Tcrt5000State(void)
 * @brief Indica el estado del perisferico, es 1 si esta leyendo y 0 si no esta leyendo
 * @param[in] ninguno
 * @return FALSE if an error occurs, in other case returns TRUE
 */

bool Tcrt5000Deinit(gpio_t dout);

/** @fn bool Tcrt5000Deinit(gpio_t dout)
 * @brief Desconecta el dispositivo
 * @param[in] gpio_t
 * @return FALSE if an error occurs, in other case returns TRUE
 */

bool Tcrt5000DownEdge(void);
/** @fn bool Tcrt5000DownEdge(void)
 * @brief Detecta flanco descendente
 * @param[in] ninguno
 * @return FALSE if an error occurs, in other case returns TRUE
 */

void DelayTcrt5000(void);
/** @fn bool DelayTcrt5000(void);
 * @brief Espera
 * @param[in] ninguno
 * @return FALSE if an error occurs, in other case returns TRUE
 */

/*==================[end of file]============================================*/
#endif /* #ifndef TCRT5000_H */

