/*
 * Valentina Avetta
 */
#ifndef HCSR04_H
#define HCSR04_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Tcrt5000
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 * This is a driver for six leds mounted on the board
 **
 **/


/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 */


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include "bool.h"
#include "gpio.h"

/*==================[macros]=================================================*/
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/** @brief Initialization function of EDU-CIAA
 * Inicializa los pines en la EDU-CIAA
 * @param[in] gpio de los puertos de entrada y salida
 * @return TRUE si no hay errores
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @brief Function de lectura de la distancia en centimetros
 *
 *	Interpreta el ancho de pulso del sensor y lo transforma una medida en centimetros
 * @return un valor de distancia en centimetro
 */
uint16_t HcSr04ReadDistanceCentimeters(void);

/** @brief Function de lectura de la distancia en pulgadas
 *
 *	Interpreta el ancho de pulso del sensor y lo transforma una medida en pulgadas
 * @return un valor de distancia en pulgadas
 */
uint16_t HcSr04ReadDistanceInches(void);

/** @brief DeInitialization function of EDU-CIAA hc_sr4
 *
 * @return TRUE if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/*==================[end of file]============================================*/
#endif /* #ifndef HCSR04_H */

