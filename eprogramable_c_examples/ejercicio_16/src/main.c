/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

/*==================[macros and definitions]=================================*/

/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare cuatro variables sin signo de 8 bits y,
 * utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
 * Realice el mismo ejercicio, utilizando la definición de una “union”. */

union allBits{
		struct cada_byte{
			uint8_t byte1;
		    uint8_t byte2;
			uint8_t byte3;
			uint8_t byte4;
		}; uint32_t  todos_los_bytes;
} union_1;

uint32_t var= 0x01020304;
uint8_t a,b,c,d;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	a= var;                /*por corrimiento de bits*/
	b= var >> 8;
	c= var >> 16;
	d= var >> 24;

	printf ("Valor variable a: %d \r\n ", a);
	printf ("Valor variable b: %d \r\n ", b);
	printf ("Valor variable c: %d \r\n ", c);
	printf ("Valor variable d: %d \r\n ", d);

	/* uso de union*/

	union_1.todos_los_bytes= 0x01020304;
	printf ("Valor con union: 0x%x \r\n ", union_1.todos_los_bytes);

	return 0;
}

/*==================[end of file]============================================*/

