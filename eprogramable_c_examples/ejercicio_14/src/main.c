/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

/*==================[macros and definitions]=================================*/

/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
Defina una variable con esa estructura y cargue los campos con sus propios datos.
Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).*/

typedef struct {
	uint8_t nombre[12];
	uint8_t apellido[20];
	uint8_t edad;
	} alumno;

uint8_t j;

alumno alumno_1;
uint8_t nombre[]= "Valentina";
uint8_t apellido[]= "Avetta";

alumno *alump;
alumno alumno_2;

/*==================[internal functions declaration]=========================*/

int main(void)
{

	alumno_1.edad= 21;
	printf ("Edad de alumno: %d \r\n", alumno_1.edad);

	for (j=0; j<12; j++){
	alumno_1.nombre[j]= nombre[j];
	}
	printf ("Nombre de alumno: %s \r\n", alumno_1.nombre);

	for (j=0; j<20; j++){
	alumno_1.apellido[j]= apellido[j];
	}
	printf ("Apellido de alumno: %s \r\n\n", alumno_1.apellido);

//-------------

	alump = &alumno_2;

	uint8_t nombre2[]= "Micaela";
	uint8_t apellido2[]= "Levrino";

	alump->edad= 22;
	printf ("Edad de alumno 2: %d \r\n", alumno_2.edad);

	for (j=0; j<20; j++){
		alump->nombre[j]= nombre2[j];}
	printf ("Nombre de alumno 2: %s \r\n", alumno_2.nombre);


	for (j=0; j<20; j++){
		alump->apellido[j]= apellido2[j];}
	printf ("Apellido de alumno 2: %s \r\n", alumno_2.apellido);

	return 0;
}

/*==================[end of file]============================================*/

