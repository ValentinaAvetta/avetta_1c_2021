/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

/*==================[macros and definitions]=================================*/
/*Escribir una función que reciba como parámetro un dígito BCD y un vector de
 * estructuras del tipo  gpioConf_t.
 *
 * Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
 * */


typedef struct {
	uint8_t port; /*!< GPIO port number */
	uint8_t pin; /*!< GPIO pin number */
	uint8_t dir; /*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

void BcdToLcd(uint8_t digitBCD, gpioConf_t *configuration) {

	uint8_t i = 0;
	//for (i = 0; i < 4; i++) { /*Para recorrer los 4 bits*/

		if (digitBCD & 1) {
			printf("Para bit %d : Se pone a 1 el pin %d del puerto %d \r \n", i,
					configuration[i].pin, configuration[i].port);
		} else {
			printf("Para bit %d : Se pone a 0 el pin %d del puerto %d \r \n", i,
					configuration[i].pin, configuration[i].port);
		}
		digitBCD = digitBCD >> 1;

	//}
}

void BinaryToBcd(uint32_t data, uint8_t digits, uint8_t *bcd_number) {
	while (digits > 0) {
		digits--;
		bcd_number[digits] = data % 10; /*da el resto*/
		data = data / 10;

	}
}

/*==================[internal functions declaration]=========================*/

int main(void) {
	uint32_t dato = 1234;
	uint8_t cantDigitos = 4;
	uint8_t numeroBinary[4];

	BinaryToBcd(dato, cantDigitos, numeroBinary);

	printf("Digito %d \r \n", numeroBinary[0]); /*Aca muestro los digitos guardados dentro del array*/
	printf("Digito %d \r \n", numeroBinary[1]);
	printf("Digito %d \r \n", numeroBinary[2]);
	printf("Digito %d \r \n", numeroBinary[3]);

	gpioConf_t config[] =
			{ { 1, 4, 1 }, { 1, 5, 1 }, { 1, 6, 1 }, { 2, 14, 1 } }; /*los 1 del tercer parametro son OUT*/

	uint8_t i = 0;
	for (i = 0; i < 4; i++) {
		BcdToLcd(numeroBinary[i], config);
	}
	return 0;
}

/*==================[end of file]============================================*/

