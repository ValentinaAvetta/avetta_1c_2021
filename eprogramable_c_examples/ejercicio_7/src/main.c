/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

/*==================[macros and definitions]=================================*/

/*Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido,
asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0 mediante máscaras y el operador << */

#define BIT3 3
#define BIT13 13
#define BIT14 14

/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr) /*Uso esta funcion para representar en binario*/
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{
	srand (time(NULL));
	uint32_t number= rand();

	printf ("Numero antes de mascara: %d \r\n", number);
	printBits (sizeof(number), &number);

	uint32_t mascara7= 0 | 1 << BIT13 | 1 << BIT14; // 00000000001100000
	mascara7= ~mascara7;                                // 11111111110011111

	uint32_t middleNumber= number & mascara7;
	uint32_t finalNumber= middleNumber | (1<<BIT3);

	printf ("Numero despues de mascaras: %d \r\n");
	printBits (sizeof(finalNumber), &finalNumber);

	return 0;
}

/*==================[end of file]============================================*/

