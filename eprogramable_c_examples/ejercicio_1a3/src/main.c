/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

/*==================[macros and definitions]=================================*/

// 1- Declare una constante de 32 bits, con todos los bits en 0 y el bit 6 en 1.Utilice el operador <<.
// 2- Declare una constante de 16 bits, con todos los bits en 0 y los bits 3 y 4 en 1. Utilice el operador <<.
// 3- Declare una variable de 16 bits sin signo, con el valor inicial 0xFFFF y luego, mediante una operación de máscaras, coloque a 0 el bit 14.


#define BIT6 6
#define BIT3 3
#define BIT4 4
#define BIT14 14

/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const * const ptr) /*Uso esta funcion para representar en binario*/
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{
   const uint32_t  a1= 1 << BIT6;
   const uint16_t  a2= 1 << BIT3 | 1 << BIT4 ;

   uint16_t mascara= 1 << BIT14;
   mascara= ~ mascara;
   uint16_t  a3= 0xFFFF & mascara;

   printf ("El valor de la variable A1 es: %d \r\n", a1);
   printf ("En binario del ej A1 es: ");
   printBits (sizeof(a1), &a1);

   printf ("\n El valor de la variable A2 es: %d \r\n", a2);
   printf ("En binario del ej A2 es:");
   printBits (sizeof(a2), &a2);

   printf ("\n El valor de la variable A3 es: %d \r\n", a3);
   printf ("En binario del ej A3 es: ");
   printBits (sizeof(a3), &a3);

	return 0;
}

/*==================[end of file]============================================*/

