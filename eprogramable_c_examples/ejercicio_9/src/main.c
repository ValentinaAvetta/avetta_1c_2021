/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

/*==================[macros and definitions]=================================*/

/*Sobre una constante de 32 bits previamente declarada,
verifique si el bit 4 es 0. Si es 0, cargue una variable “A” previamente declarada en 0,
si es 1, cargue “A” con 0xaa.*/

#define BIT4 4

/*==================[internal functions declaration]=========================*/

void printBits(size_t const size, void const *const ptr) /*Uso esta funcion para representar en binario*/
{
	unsigned char *b = (unsigned char*) ptr;
	unsigned char byte;
	int i, j;

	for (i = size - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			byte = (b[i] >> j) & 1;
			printf("%u", byte);
		}
	}
	puts("");
}

int main(void) {
	srand(time(NULL));
	uint32_t number = rand();

	printf("Numero antes de mascara: %d \r\n", number);
	printBits(sizeof(number), &number);

	const uint32_t mask = 1 << BIT4;
	uint32_t finalNumber = number & mask;

	printf("Numero mascara: %d \r\n", mask);
	printBits(sizeof(mask), &mask);

	printf("Numero final: %d \r\n", finalNumber);
	printBits(sizeof(finalNumber), &finalNumber);

	uint32_t A = 0;

	if (finalNumber == 0) {
		A = 0;
		printf("\n El valor de la variable A es: %d \r\n", A);
	} else {
		A = 0xaa;
		printf("\n El valor de la variable A es: %d \r\n", A);
	}

	return 0;
}

/*==================[end of file]============================================*/

