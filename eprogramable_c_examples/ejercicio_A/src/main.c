/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

/*==================[macros and definitions]=================================*/
//Realice un función que reciba un puntero a una estructura LED como la que se muestra a continuación:


typedef enum { /*banderas modo*/
	ON = 1, /*pongo uno los demas se enumeran solo, o sea off es 2 y toggle es 3*/
	OFF, TOGGLE,
} state_t;

typedef enum { /*banderas nuemro de led*/
	LED1 = 1, LED2, LED3,
} led_t;

typedef struct {
	uint8_t n_led; /* indica el número de led a controlar*/
	uint8_t n_ciclos; /* indica la cantidad de ciclos de encendido/apagado*/
	uint8_t periodo; /* indica el tiempo de cada ciclo. ES RETARDO EN EL DIAGRAMA*/
	uint8_t mode; /* ON, OFF, TOGGLE (en toggle tengo a ciclos y a periodos)*/
} leds;

void LedControl(leds *l); /*declaro funcion*/

/*==================[internal functions declaration]=========================*/

void LedControl(leds *l) {

	uint8_t i;

	switch (l->mode) {

	case ON: /*MODO ON*/
		switch (l->n_led) {
		case LED1:
			printf("Se enciende %d", LED1);
			break;
		case LED2:
			printf("Se enciende %d", LED2);
			break;
		case LED3:
			printf("Se enciende %d", LED3);
			break;
		}
		break;

	case OFF: /*MODO OFF*/
		switch (l->n_led) {
		case LED1:
			printf("Se apaga %d", LED1);
			break;
		case LED2:
			printf("Se apaga %d", LED2);
			break;
		case LED3:
			printf("Se apaga %d", LED3);
			break;
		}
		break;

	case TOGGLE: /*MODO TOGGLE*/
		for (i = 0; i < l->n_ciclos; i++) {
			switch (l->n_led) {
			case LED1:
				printf("Se togglea %d", LED1);
				break;
			case LED2:
				printf("Se togglea %d", LED2);
				break;
			case LED3:
				printf("Se togglea %d", LED3);
				break;
			}
			for (i = 0; i < l->periodo; i++) { /*no hace nada solo espera a que se compla el periodo*/
			}
			break;
		}
	}
}

int main(void) {
	leds miLed = { LED1, 10, 200, TOGGLE };
	LedControl(&miLed);

	return 0;
}

/*==================[end of file]============================================*/

